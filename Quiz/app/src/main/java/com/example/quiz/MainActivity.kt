package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.quiz.databinding.ActivityMainBinding
import com.example.quiz.databinding.FragmentMainmenuBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
/*
        supportFragmentManager.beginTransaction().apply {
            replace(binding.FragmentContainerView.id, Mainmenu())
            commit()
        }

        binding.playbutton.setOnClickListener {
            val ask = Intent(this, Help::class.java)
            startActivity(ask)
        }


        binding.helpbutton.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this@MainActivity).create()
            alertDialog.setTitle("HOW TO PLAY")
            alertDialog.setMessage("Pulsa el botón PLAY y te llevará a una serie de preguntas. Hay diferentes categorías con distintas preguntas cada una. Se mostrará la puntuación final una vez finalizado el juego.")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "EXIT"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }

 */

    }
}

/* package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContentProviderCompat.requireContext
import com.example.quiz.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


    }
}
*/