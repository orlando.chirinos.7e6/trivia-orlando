package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout.Directions
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.quiz.databinding.FragmentMainmenuBinding

class Mainmenu : Fragment(R.layout.fragment_mainmenu) {

    lateinit var binding: FragmentMainmenuBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainmenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.playbutton.setOnClickListener {
            val action = MainmenuDirections.actionMainmenu2ToGame()
            findNavController().navigate(action)
        }
        binding.rankingbutton.setOnClickListener {
            val action = MainmenuDirections.actionMainmenu2ToRanking2()
            findNavController().navigate(action)
        }

        binding.helpbutton.setOnClickListener {
            val alertDialog = AlertDialog.Builder(requireContext()).create()
            alertDialog.setTitle("HOW TO PLAY")
            alertDialog.setMessage("Pulsa el botón PLAY y te llevará a una serie de preguntas. Hay diferentes categorías con distintas preguntas cada una. Se mostrará la puntuación final una vez finalizado el juego.")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "EXIT"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }
    }
}


    //override fun onCreate(savedInstanceState: Bundle?) {

      //  setContentView(binding.root)





//}

/*
package com.example.quiz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Mainmenu.newInstance] factory method to
 * create an instance of this fragment.
 */
class Mainmenu : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mainmenu, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Mainmenu.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Mainmenu().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

 */